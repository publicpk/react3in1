/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';

import Modules from './modules';

const App = () => (
    <MuiThemeProvider muiTheme={getMuiTheme(baseTheme)}>
        <Router>
            <Modules />
        </Router>
    </MuiThemeProvider>
);

export default App;
