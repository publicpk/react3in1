import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import injectTapEventPlugin from 'react-tap-event-plugin';

import App from './App'
import Store from './Store';
import registerServiceWorker from './utils/registerServiceWorker';

// Needed for onTouchTap * at the TOP *
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(
    <Provider store={Store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
