/**
 * Created by peterk on 7/11/17.
 */

import { combineReducers } from 'redux';

import coreReducer from './coreReducer';
import { reducerInfos as moduleReducerInfos } from '../modules';

//
// combine reducers
const reducers = { coreReducer };
moduleReducerInfos.forEach((redInfo) => (
    redInfo && (reducers[redInfo.id] = redInfo.reducer)
));
console.log('Current reducers: ');
console.log(moduleReducerInfos);
console.log(reducers);

const Reducers = combineReducers(reducers);

export default Reducers;
