/**
 * Created by peterk on 7/11/17.
 */
import { connect } from 'react-redux';

import { REDUCER_ID } from '../constants/actionTypes';
import { actions } from '../actions/actions';
import Home from '../components/Home'

const mapHomeStateToProps = (state) => {
    // console.log(state);
    return {
        reducer: state[REDUCER_ID]
    };
};

const mapHomeDispatchToProps = (dispatch) => ({
    add: (val) => dispatch(actions.add(val))
});

const HomeContainer = connect(mapHomeStateToProps, mapHomeDispatchToProps)(Home);

export default HomeContainer;
