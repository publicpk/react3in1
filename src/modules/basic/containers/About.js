/**
 * Created by peterk on 7/11/17.
 */

import { connect } from 'react-redux';

import { REDUCER_ID } from '../constants/actionTypes';
import About from '../components/About'

const mapAboutStateToProps = (state) => ({
    reducer: state[REDUCER_ID]
});
const AboutContainer = connect(mapAboutStateToProps)(About);

export default AboutContainer;
