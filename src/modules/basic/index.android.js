
import Frame from './components/Frame-native';
import reducerInfo from './reducers';

const reducerInfos = [
    reducerInfo
];
export { reducerInfos }

export default Frame;
