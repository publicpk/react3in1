import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Frame from './components/Frame';
import reducerInfo from './reducers';

const routes = [
    {name: 'Basic', path: '', component: Frame},
];

const Module = ({match}) => {
    const {url: base} = match;
    return (
        <Switch>
            {routes.map((route, i) => (
                <Route key={i} path={base+route.path} component={route.component} exact={route.exact}/>
            ))}
        </Switch>
    );
};
const reducerInfos = [
    reducerInfo
];
export { reducerInfos }
export default Module;
