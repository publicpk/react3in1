/**
 * Created by peterk on 7/11/17.
 */
import { HomeContainer, AboutContainer } from '../containers';
import { ModulePreview } from '../components';

console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>');
console.log(ModulePreview);

//
// route info
//
const routes = [
    {label: 'Home', exact: true, path:'', component:HomeContainer},
    {label: 'About', path:'/about/:value', component:AboutContainer},
    {label: 'More...', path: '/modules', component:ModulePreview}
];

export default routes;
