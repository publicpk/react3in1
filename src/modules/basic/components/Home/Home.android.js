/**
 * Created by peterk on 7/11/17.
 */
import React from 'react';
import {Platform, StyleSheet, Text, TouchableNativeFeedback, View} from 'react-native';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this._onPressIncreaseButton = this._onPressIncreaseButton.bind(this);
        this._onPressDecreaseButton = this._onPressDecreaseButton.bind(this);
    }

    static navigationOptions = {
        tabBarLabel: 'Home',
        title: 'Home'
    };

    _onPressIncreaseButton() {
        const {add} = this.props;
        add(1);
    }

    _onPressDecreaseButton() {
        const {reducer, add} = this.props;
        const {navigate} = this.props.navigation;
        add(-1);
        navigate('About', {value: reducer ? reducer.count : '', reducer: reducer});
    }

    render() {
        const {reducer} = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Home</Text>
                <Text>Current count: {reducer ? reducer.count : 'no reducer!!!'}</Text>
                <TouchableNativeFeedback onPress={this._onPressIncreaseButton} background={buttonBackground}>
                    <View style={styles.button}><Text style={styles.buttonText}>+1</Text></View>
                </TouchableNativeFeedback>
                <TouchableNativeFeedback onPress={this._onPressDecreaseButton} background={buttonBackground}>
                    <View style={styles.button}><Text style={styles.buttonText}>-1</Text></View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}

const buttonBackground = Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : '';
const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        padding: 10
    },
    title: {
        fontSize: 40,
        fontWeight: 'bold',
        padding: 10
    },
    button: {
        borderColor: 'black',
        borderWidth: 0.5,
        alignItems: 'center',
        backgroundColor: 'lightgrey'
    },
    buttonText: {
        padding: 10,
        color: 'black'
    }
});

export default Home;
