/**
 * Created by peterk on 7/11/17.
 */
import React, { Component } from 'react';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this._onPressIncreaseButton = this._onPressIncreaseButton.bind(this);
        this._onPressDecreaseButton = this._onPressDecreaseButton.bind(this);
    }

    componentWillMount() {
    }

    _onPressIncreaseButton() {
        const {add} = this.props;
        add(1);
    }

    _onPressDecreaseButton() {
        const {reducer, add, match, history } = this.props;
        add(-1);
        // go to about page with param
        const url = `${match.url}/about/${reducer.count}`;
        history.push(url);
        // TOFIX: tab location
    }

    render() {
        const {reducer} = this.props;
        return (
            <div>
                <h1>Home</h1>
                <p>Current count: {reducer ? reducer.count : 'no reducer!!!'}</p>
                <button onClick={this._onPressIncreaseButton}>+1</button>
                <button onClick={this._onPressDecreaseButton}>-1</button>
            </div>
        );
    }
}

export default Home;
