
import React from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import { withRouter } from 'react-router-dom';

import MainTab from './MainTab';
import LeftDrawerItems from './LeftDrawerItems';

class MainAppBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLeftDrawer: false
        };
        this.base = props.match ? props.match.url : '/';
        //this.handleCloseLeftDrawer = this.handleCloseLeftDrawer.bind(this);
    }

    handleToggleLeftDrawer = () => this.setState({showLeftDrawer: !this.state.showLeftDrawer});

    handleCloseLeftDrawer = () => this.setState({showLeftDrawer: false});

    handleOpenLeftDrawer = () => this.setState({showLeftDrawer: true});

    render() {
        return (
            <AppBar style={styles.appBar}
                    onLeftIconButtonTouchTap={(event)=>this.handleToggleLeftDrawer()}
            >
                <MainTab base={this.base} location={this.props.location} openLeftDrawer={this.handleOpenLeftDrawer}/>

                <Drawer width={200}
                        docked={false}
                        open={this.state.showLeftDrawer}
                        onRequestChange={(showLeftDrawer) => this.setState({showLeftDrawer})}>
                    <LeftDrawerItems handleClose={this.handleCloseLeftDrawer}/>
                </Drawer>
            </AppBar>
        );
    }
}

var styles = {
    appBar: {
    },
};


export default withRouter(MainAppBar);