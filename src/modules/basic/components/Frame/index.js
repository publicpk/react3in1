import React from 'react';
import {Switch, Route} from 'react-router-dom';

import MainAppBar from './MainAppBar';
import routes from '../../routes';
import { NotFound } from '../misc';

const Frame = ({match}) => {
    const {url: base} = match;
    return (
        <div>
            <MainAppBar/>
            <Switch>
                {routes.map((route, i) => (
                    <Route key={i} path={base+route.path} component={route.component} exact={route.exact}/>
                ))}
                <Route component={NotFound}/>
            </Switch>
        </div>
    );
};

export default Frame;
