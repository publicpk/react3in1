
import React from 'react';
import {Link} from 'react-router-dom';
import {Tabs, Tab} from 'material-ui/Tabs';
import routes from '../../routes';

class MainTab extends React.Component {
    constructor(props) {
        super(props);
        this.base = props.base;
        this.tabItems = routes.map((item) => {
            if ( !item.label ) return '';
            return (
                item.label !== 'More...' ?
                    <Tab key={item.path} label={item.label} containerElement={<Link to={this.base + item.path}/>}/> :
                    <Tab key={item.path} label={item.label} onActive={props.openLeftDrawer} containerElement={<Link to={this.base + item.path}/>}/>
            );
        });
    }

    render() {
        let {pathname} = this.props.location;
        let selectedIdx = -1;

        if (pathname) {
            // the pathname shoud start with the value of this.base
            pathname = pathname.substring(this.base.length);
            for (let i = routes.length-1; i >= 0; i --) {
                if ( pathname === routes[i].path ) {
                    selectedIdx = i;
                    break;
                }
            }
            // The following 'routes.entries()' does not work in the IE 11
            // for (const [idx, val] of routes.entries()) {
            //     if (pathname === val.path) {
            //         selectedIdx = idx;
            //         break;
            //     }
            // }
        }
        return (
            <Tabs style={styles.tabs} initialSelectedIndex={selectedIdx}>
                {this.tabItems}
            </Tabs>);
    }
}

var styles = {
    tabs: {
        width: '100%'
    }
};

export default MainTab;
