import React from 'react';
import MenuItem from 'material-ui/MenuItem';
import IconRemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import Divider from 'material-ui/Divider';

//import Modules from '../../mods';

const LeftDrawerItems = (props) => (
    <div>
        <MenuItem primaryText="Modules..." leftIcon={<IconRemoveRedEye/>} onTouchTap={props.handleClose}/>
        <Divider />
        {/*<Modules/>*/}
    </div>
);

export default LeftDrawerItems;
