import React from 'react';
import { Button, Platform, ScrollView, StyleSheet } from 'react-native';
import { TabNavigator, DrawerNavigator } from 'react-navigation';

import routes from '../../routes';

const RouteConfigs = {};
routes.forEach((route, i) => (
    RouteConfigs[route.label] = {
        screen: route.component,
        path: route.path
    }
));

//
// tab navigator
const TabNaviConfig = {
    activeTintColor: Platform.OS === 'ios' ? '#e91e63' : '#fff'
};

const TabLayout = TabNavigator(
    RouteConfigs,
    TabNaviConfig
);

const DrawerNaviRouteConfig = {
    TabLayout: {
        screen: TabLayout
    }
    
};

const Frame = DrawerNavigator(DrawerNaviRouteConfig);

export default Frame;
