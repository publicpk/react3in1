import { DrawerNavigator } from 'react-navigation';

import routes from '../../routes';

const RouteConfig = {};
routes.forEach((route, i) => (
    RouteConfig[route.label] = {
        screen: route.component,
        path: route.path
    }
));

//
// drawer navigator
const Layout = DrawerNavigator(RouteConfig);

export default Layout;
