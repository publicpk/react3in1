
import { Platform } from 'react-native';
import { StackNavigator } from 'react-navigation';

import routes from '../../routes';

const RouteConfigs = {};
routes.forEach((route, i) => (
    RouteConfigs[route.label] = {
        screen: route.component,
        path: route.path
    }
));

//
// stack navigator
const StackNavigatorConfig = {
   mode: 'card',
   headerMode: 'screen'
};
const Layout = StackNavigator(RouteConfigs, StackNavigatorConfig);

export default Layout;
