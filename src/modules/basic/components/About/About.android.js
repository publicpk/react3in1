/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

const About = ({reducer, navigation}) => {
    const {params} = navigation.state;
    return (
        <View style={styles.container}>
            <Text style={styles.title}>About {params ? `with param, ${params.value}` : ''}</Text>
            <Text>Current count: {reducer ? reducer.count : 'no reducer!!!'}</Text>
        </View>
    );
};

About.navigationOptions = (props) => {
    // console.log(props);
    return {
        tabBarLabel: 'About',
        title: 'About'
    }};

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        padding: 10,
        alignItems: 'center'
    },
    title: {
        fontSize: 40,
        fontWeight: 'bold',
        padding: 10
    }
});

export default About;
