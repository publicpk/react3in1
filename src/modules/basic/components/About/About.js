/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';

const About = ({reducer, match}) => {
    const { params } = match;
    return (
        <div>
            <h1>About {params ? `with param, ${params.value}` : ''}</h1>
            <p>Current count: {reducer ? reducer.count : 'no reducer!!!'}</p>
        </div>
    );
};

export default About;
