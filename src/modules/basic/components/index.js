import Home from './Home';
import About from './About'
import { ModulePreview, NotFound } from './misc';

export {
    Home,
    About,
    ModulePreview,
    NotFound
}
