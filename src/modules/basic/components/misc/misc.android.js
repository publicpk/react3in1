/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const NotFound = () => (
    <View>
        <Text>Not Found</Text>
    </View>
);

const ModulePreview = ({navigation}) => (
    <View style={styles.container}>
        <Text>Select a module from the LEFT nav</Text>
    </View>
);

ModulePreview.navigationOptions = {
    tabBarLabel: 'Modules',
    title: 'Modules'
};

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        padding: 10
    }
});

export {
    NotFound,
    ModulePreview
};
