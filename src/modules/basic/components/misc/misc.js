/**
 * Created by peterk on 7/11/17.
 */

import React from 'react';

const NotFound = () => (
    <div>
        <h1>Not Found</h1>
    </div>
);

const ModulePreview = () => (
    <div>
        <h2>Select a module from the LEFT nav</h2>
    </div>
);

export {
    NotFound,
    ModulePreview
};
