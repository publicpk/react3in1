/**
 * Created by peterk on 7/11/17.
 */
export const REDUCER_ID = 'basic-reducer';

export const ADD = `${REDUCER_ID}.ADD`;
