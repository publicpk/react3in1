
import { ActionTypes } from '../constants'

const reducer = (state = {count: 0}, action) => {
    switch ( action.type ) {
        case ActionTypes.ADD:
            return {...state, count: state.count + action.value || 0};
        default:
            return state;
    }
};

export default {
    id: ActionTypes.REDUCER_ID,
    reducer
};
