
import { ActionTypes } from '../constants';

const add = (value) => ({
    type: ActionTypes.ADD,
    value: value
});

const actions = {
    add
};

export {actions};
export default actions;
