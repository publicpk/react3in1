import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import ModHello, {reducerInfo as helloReducerInfo} from './hello';
import ModWorld, {reducerInfo as worldReducerInfo} from './world';

const RedirectTo = ({to}) => <Redirect to={to}/>;

const routes = [
    {name: 'Hello', path: '/hello', component: ModHello},
    {name: 'World', path: '/world', component: ModWorld},
];

const ModRouterRedux = ({match}) => {
    const {url: base} = match;
    return (
        <Switch>
            {routes.map((route, i) => (
                <Route key={i} path={base+route.path} component={route.component} exact={route.exact}/>
            ))}
            <Route path={base} render={props => (
                <RedirectTo {...props} to={base + '/hello'}/>
            )}/>
        </Switch>
    );
};

const reducerInfo = [
    helloReducerInfo,
    worldReducerInfo
];

export { reducerInfo }
export default ModRouterRedux;
