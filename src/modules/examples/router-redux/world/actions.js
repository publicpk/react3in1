//
// action
//
let count = 0;
const increase = () => ({
    type: 'world.INCREASE',
    count: ++count
});
const add = (value) => ({
    type: 'world.ADD',
    value: value
});

export default {
    increase,
    add
};
