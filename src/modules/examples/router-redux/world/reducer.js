
//
// reducer
//
const reducerId = 'router-redux-world-reducer';

const reducer = (state = {count: 0}, action) => {
    switch ( action.type ) {
        case 'world.INCREASE':
            return {...state, count: action.count};
        case 'world.ADD':
            return {...state, count: state.count + action.value};
        default:
            return state;
    }
};

export default {
    id: reducerId,
    reducer: reducer
}
