//
// action
//
let count = 0;
const increase = () => ({
    type: 'hello.INCREASE',
    count: ++count
});
const add = (value) => ({
    type: 'hello.ADD',
    value: value
});

export default {
    increase,
    add
};
