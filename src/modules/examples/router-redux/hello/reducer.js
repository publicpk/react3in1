
//
// reducer
//
const reducerId = 'router-redux-hello-reducer';

const reducer = (state = {count: 0}, action) => {
    switch ( action.type ) {
        case 'hello.INCREASE':
            return {...state, count: action.count};
        case 'hello.ADD':
            return {...state, count: state.count + action.value};
        default:
            return state;
    }
};

export default {
    id: reducerId,
    reducer: reducer
}
