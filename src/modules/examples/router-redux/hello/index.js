import React from 'react';
import {Route, Link, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';

import actions from './actions';
import reducerInfo from './reducer';

//
// redux connector
//
const mapStateToProps = (state) => ({
    reducer: state[reducerInfo.id]
});

const mapDispatchToProps = (dispatch) => ({
    increase: () => dispatch(actions.increase()),
    add: (val) => dispatch(actions.add(val))
});

//
// components
//
const Home = ({reducer, increase, add}) => (
    <div><h1>Hello~</h1>
        <p>Counter: {reducer.count}</p>
        <button onClick={() => increase()}> INCREASE(+1) </button>
        <button onClick={() => add(-1)}> DECREASE(-1) </button>
    </div>);
const Articles = () => <h1>Hello Articles</h1>;
const About = () => <h1>About Hello</h1>;
const RedirectTo = ({ to }) => <Redirect to={to}/>;
const EntityCir = () => <span>&#x25CB;&nbsp;</span>

//
// containers
//
const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Home);

const routes = [
    {name: 'HOME', exact: true, path: '', component: HomeContainer},
    {name: 'Articles', path: '/articles', component: Articles},
    {name: 'About', path: '/about', component: About},
    {name: 'Go to Home', path: '/home', compoent: RedirectTo}
];

const HelloComponent = (props) => {
    const {url: base} = props.match;
    const parent = base.substring(0, base.lastIndexOf('/'));
    // const {reducer, increase, add} = props;
    return (
        <div>
            <h3 > Hello Module </h3>
            <ul >
                <li style={style.routeLink}><EntityCir/><Link to={base+routes[0].path}>{routes[0].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityCir/><Link to={base+routes[1].path}>{routes[1].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityCir/><Link to={base+routes[2].path}>{routes[2].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityCir/><Link to={base+routes[3].path}>{routes[3].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityCir/><Link to={parent + '/world'}> Go to WORLD </Link>&nbsp;</li>
            </ul>
            <Route path={base+routes[0].path} component={routes[0].component} exact={routes[0].exact}/>
            <Route path={base+routes[1].path} component={routes[1].component} exact={routes[1].exact}/>
            <Route path={base+routes[2].path} component={routes[2].component} exact={routes[2].exact}/>
            <Route path={base+routes[3].path} exact={routes[3].exact} render={props => (
                <RedirectTo {...props} to={base}/>
            )}/>
        </div>
    );
};

const style = {
    routeLink: {
        display: 'inline'
    }
};

//
// container
//
const HelloContainer = connect(mapStateToProps, mapDispatchToProps)(HelloComponent);

// export reducer info and container
export {reducerInfo};

export default HelloContainer;
