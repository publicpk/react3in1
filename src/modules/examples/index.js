import React from 'react';
import {Switch, Route, Link} from 'react-router-dom';

import ModSimpleRouter from './simple-router';
import ModSimpleRedux, {reducerInfo as modSimpleReduxReducerInfo} from './simple-redux';
import ModRouterRedux, {reducerInfo as modRouterReduxReducerInfo} from './router-redux';

//
// routes
const routes = [
    {name: 'Simple Router', path: '/simple-router', component: ModSimpleRouter},
    {name: 'Simple Redux', path: '/simple-redux', component: ModSimpleRedux},
    {name: 'Router Redux', path: '/router-redux', component: ModRouterRedux},
];

//
// Example indices
const ExampleIndices = ({base}) => (
    <ul>
        {routes.map((route, i) => (
            <li key={i}><Link to={base+route.path}>{route.name}</Link></li>
        ))}
    </ul>
);
// routes.unshift({name: 'Example Indices', path: '', component: ExampleIndices});

//
// define modules
const Modules = ({match}) => {
    const {url: base} = match;
    return (
        <div>
            <ExampleIndices base={base}/> <hr/>
            <Switch>
                {routes.map((route, i) => (
                    <Route key={i} path={base+route.path} component={route.component} exact={route.exact}/>
                ))}
            </Switch>
        </div>
    );
};

const reducerInfo = [
    ...modSimpleReduxReducerInfo,
    ...modRouterReduxReducerInfo
];

export { reducerInfo }
export default Modules;
//export default () => <div/>;
