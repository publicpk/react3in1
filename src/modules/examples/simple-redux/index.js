import React from 'react';
import { connect } from 'react-redux';

//
// action
//
let count = 0;
const action_increase = () => ({
    type: 'simple.INCREASE',
    count: ++count
});
const action_add = (value) => ({
    type: 'simple.ADD',
    value: value
});

//
// reducer
//
const reducer = (state = {count: 0}, action) => {
    switch ( action.type ) {
        case 'simple.INCREASE':
            return {...state, count: action.count};
        case 'simple.ADD':
            return {...state, count: state.count + action.value};
        default:
            return state;
    }
};

//
// component
//
const ModReduxComponent = (props) => {
    const {reducer, increase, add} = props;
    return (
        <div>
            <h3 > Simple Redux </h3> <hr/>
            <p> Current count is {reducer.count}</p>
            <button onClick={() => increase()}> INCREASE(+1) </button>
            <button onClick={() => add(-1)}> DECREASE(-1) </button>
        </div>
    );
};

//
// container
//
const reducerId = 'simple-redux-reducer';
const reducerInfo = [{
    id: reducerId,
    reducer: reducer
}];

const mapStateToProps = (state) => ({
    reducer: state[reducerId]
});

const mapDispatchToProps = (dispatch) => ({
    increase: () => dispatch(action_increase()),
    add: (val) => dispatch(action_add(val))
});

const ModReduxContainer = connect(mapStateToProps, mapDispatchToProps)(ModReduxComponent);


// export reducer info and container
export {reducerInfo};
export default ModReduxContainer;
