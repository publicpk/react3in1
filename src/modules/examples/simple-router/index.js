import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import ModHello from './hello';
import ModWorld from './world';

const RedirectTo = ({to}) => <Redirect to={to}/>;

const routes = [
    {name: 'Hello', path: '/hello', component: ModHello},
    {name: 'World', path: '/world', component: ModWorld},
];

const ModSimpleRouter = ({match}) => {
    const {url: base} = match;
    return (
        <Switch>
            {routes.map((route, i) => (
                <Route key={i} path={base+route.path} component={route.component} exact={route.exact}/>
            ))}
            <Route path={base} render={props => (
                <RedirectTo {...props} to={base + '/hello'}/>
            )}/>
        </Switch>
    );
};

const reducerInfos = [];

export { reducerInfos }
export default ModSimpleRouter;
