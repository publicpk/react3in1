import React from 'react';
import {Route, Link, Redirect} from 'react-router-dom';

const Home = () => <h1>World~</h1>;
const Articles = () => <h1>World Articles</h1>;
const About = () => <h1>About World</h1>;
const RedirectTo = ({ to }) => <Redirect to={to}/>;
const EntityBull = () => <span>&bull;&nbsp;</span>

const routes = [
    {name: 'HOME', exact: true, path: '', component: Home},
    {name: 'Articles', path: '/articles', component: Articles},
    {name: 'About', path: '/about', component: About},
    {name: 'Go to Home', path: '/home', component: RedirectTo}
];

const ModWorld = ({ match }) => {
    const base = match.url;
    const parent = base.substring(0, base.lastIndexOf('/'));
    return (
        <div>
            <h3> World Module </h3>
            <ul>
                <li style={style.routeLink}><EntityBull/><Link to={base+routes[0].path}>{routes[0].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityBull/><Link to={base+routes[1].path}>{routes[1].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityBull/><Link to={base+routes[2].path}>{routes[2].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityBull/><Link to={base+routes[3].path}>{routes[3].name}</Link>&nbsp;</li>
                <li style={style.routeLink}><EntityBull/><Link to={parent + '/hello'}> Go to HELLO </Link>&nbsp;</li>
            </ul>
            <Route path={base+routes[0].path} component={routes[0].component} exact={routes[0].exact}/>
            <Route path={base+routes[1].path} component={routes[1].component} exact={routes[1].exact}/>
            <Route path={base+routes[2].path} component={routes[2].component} exact={routes[2].exact}/>
            <Route path={base+routes[3].path} exact={routes[3].exact} render={props => (
                <RedirectTo {...props} to={base}/>
            )}/>
        </div>
    );
};

const style = {
    routeLink: {
        display: 'inline'
    }
};

export default ModWorld;
