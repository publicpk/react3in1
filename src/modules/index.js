
import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';

import BasicModule, {reducerInfos as baseReducerInfos} from './basic';
import ExampleModule, {reducerInfo as exampleReducerInfo} from './examples';

//
// routes
const routes = [
    {name: 'Base', path: '/base', component: BasicModule},
    {name: 'Examples', path: '/examples', component: ExampleModule},
];

//
// module path for '/'
const mainModulePath = routes[0].path;

//
// define modules
const RouteObject = routes.map((route, i) => (
    <Route key={i} path={route.path} component={route.component} exact={route.exact}/>
));
const Modules = () => (
    <Switch>
        {RouteObject}
        <Route path='/' render={props => (
                <Redirect to={mainModulePath}/>
            )}/>
    </Switch>
);

//
// reducer information
const reducerInfos = [
    ...baseReducerInfos,
    ...exampleReducerInfo,
];

//
// export
export { reducerInfos }
export default Modules;

// export empty
// export let reducerInfo = [];
// export default () => <div/>;
