
import BasicModule, {reducerInfos as baseReducerInfos} from './basic';

const reducerInfos = [
    ...baseReducerInfos
];

export { reducerInfos }
export default BasicModule;
